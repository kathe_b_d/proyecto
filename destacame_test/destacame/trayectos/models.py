from django.db import models
from buses.models import Bus
from pasajeros.models import Pasajero

# Create your models here.
class Trayecto(models.Model):
    id_trayecto = models.AutoField(primary_key=True)
    origen_trayecto = models.CharField(max_length=100, verbose_name="Origen Trayecto")
    destino_trayecto = models.CharField(max_length=500, verbose_name="Destino Trayecto")
    duracion = models.IntegerField(verbose_name='Duración')
    fecha_creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    fecha_actualizado = models.DateTimeField(auto_now=True, verbose_name="Fecha de actualización")

    class Meta: 
        verbose_name = 'trayecto'
        verbose_name_plural = 'trayectos'
        ordering = ['-fecha_creado']

    def promedio(self):
        trayectos=Trayecto_Bus.objects.all().filter(id_trayecto=self.id_trayecto)
        cantidad = trayectos.count()
        cantidadp =0
        for trayecto in trayectos:
           cantidadp =cantidadp+ Bus_Pasajero.objects.all().filter(id_bus_trayecto=trayecto.id_trayecto_bus).count()

        if(cantidad==0):
            return 0
        return cantidadp/cantidad

    def _str_(self):
        return self.origen_trayecto

class Trayecto_Bus(models.Model):
    id_trayecto_bus = models.AutoField(primary_key=True)
    id_trayecto = models.ForeignKey(Trayecto, on_delete=models.CASCADE)
    id_bus = models.ForeignKey(Bus, on_delete=models.CASCADE)
    horario = models.TimeField(auto_now=False, auto_now_add=False, verbose_name ="Horario")

    class Meta: 
        verbose_name = 'trayecto_bus'
        verbose_name_plural = 'trayectos_buses'

            

    def _str_(self):
        return self.id_trayecto.origen_trayecto

class  Bus_Pasajero(models.Model):
    id_bus_pasajero = models.AutoField(primary_key=True)
    id_bus_trayecto = models.ForeignKey(Trayecto_Bus, on_delete=models.CASCADE)
    id_pasajero = models.ForeignKey(Pasajero, on_delete=models.CASCADE)
    asiento = models.CharField(max_length=3, verbose_name='Asiento')
    fecha = models.DateField(auto_now=False, auto_now_add=False, verbose_name= 'Fecha')

    def _str_(self):
        return self.id_bus_pasajero

    def capacidad(self):
        capacidad_=Bus_Pasajero.objects.all().filter(id_bus_trayecto=self.id_bus_trayecto).filter(fecha=self.fecha).count()
        if(capacidad_==0):
            return 100.00
        return (capacidad_*100)/10

    def _id_trayecto(self):
        return self.id_bus_trayecto.id_trayecto_bus

