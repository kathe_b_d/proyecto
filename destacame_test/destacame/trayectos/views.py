from rest_framework import generics,status
from rest_framework.decorators import action
from .models import Trayecto,Trayecto_Bus,Bus_Pasajero
from .serializers import TrayectoSerializer,TrayectoBusSerializer,TrayectoBusInfoSerializer,Bus_PasajeroSerializer  ,Bus_PasajeroSerializer2,TrayectoSerializer2,TrayectoBusSerializer2
from rest_framework import viewsets
import pprint
from rest_framework.response import Response

# Create your views here.

class TrayectoViewSet(viewsets.ModelViewSet):
    
    serializer_class= TrayectoSerializer

    def get_queryset(self):
        queryset = Trayecto.objects.all()
        origen = self.request.query_params.get('origen', None)
        destino = self.request.query_params.get('destino', None)
        if origen is not None:
            queryset = Trayecto.objects.filter(origen_trayecto=origen).filter(destino_trayecto=destino)
        return queryset

    def create(self,request):
        if(self.request.data['origen_trayecto']==self.request.data['destino_trayecto']):
           return Response({ "origen_trayecto": ["Este campo no puede ser igual al Destino."]}, status=status.HTTP_404_NOT_FOUND)
        
        serializer = TrayectoSerializer(data=request.data)
        if serializer.is_valid():
            self.object = serializer.save()
            headers = self.get_success_headers(serializer.data)
                
            return Response(serializer.data, status=status.HTTP_200_OK)
    
    
class TrayectoViewSet2(viewsets.ModelViewSet):
    queryset = Trayecto.objects.all()
    serializer_class= TrayectoSerializer2   



class TrayectoBusViewSet(viewsets.ModelViewSet):
    queryset = Trayecto_Bus.objects.all()
    serializer_class= TrayectoBusSerializer   

    def get_queryset(self):
        queryset = Trayecto_Bus.objects.all()
        id_trayecto = self.request.query_params.get('id_trayecto', None)
        if id_trayecto is not None:
            queryset = Trayecto_Bus.objects.filter(id_trayecto=id_trayecto) 
        return queryset   


class TrayectoBusViewSet2(viewsets.ModelViewSet):
    queryset = Trayecto_Bus.objects.all()
    serializer_class= TrayectoBusSerializer2   



class BusPasajeroViewSet(viewsets.ModelViewSet):
    
    serializer_class= Bus_PasajeroSerializer2
    
    def get_queryset(self):
        queryset = Bus_Pasajero.objects.all()
        id_pasajero = self.request.query_params.get('id_pasajero', None)
        if id_pasajero is not None:
            queryset = Bus_Pasajero.objects.filter(id_pasajero=id_pasajero) 
        return queryset
    
    def list(self, request):   
        queryset = Bus_Pasajero.objects.all()
        serializer = Bus_PasajeroSerializer2(queryset, many=True)
        
        new_serializer= list(serializer.data)
        my_list = []
        my_list_fecha = []
        for valor in new_serializer :
            print(valor['_id_trayecto'] + "-"+ valor['fecha'])
            if not (valor['_id_trayecto'] + "-"+ valor['fecha']) in my_list_fecha :
                my_list.append(valor)
                my_list_fecha.append(valor['_id_trayecto'] + "-"+ valor['fecha'])

        return Response(my_list)


class BusPasajeroViewSet_2(viewsets.ModelViewSet):
    
    serializer_class= Bus_PasajeroSerializer2
    
    def get_queryset(self):
        queryset = Bus_Pasajero.objects.all()
        id_pasajero = self.request.query_params.get('id_pasajero', None)
        if id_pasajero is not None:
            queryset = Bus_Pasajero.objects.filter(id_pasajero=id_pasajero) 
        return queryset


class BusPasajeroNViewSet(viewsets.ModelViewSet):
    
    serializer_class= Bus_PasajeroSerializer
    

    def get_queryset(self):
        queryset = Bus_Pasajero.objects.all()
        id_bus_trayecto = self.request.query_params.get('id_bus_trayecto', None)
        fecha = self.request.query_params.get('fecha', None)
        if id_bus_trayecto is not None and True:
            queryset = Bus_Pasajero.objects.filter(id_bus_trayecto=id_bus_trayecto).filter(fecha=fecha)
        
        return queryset

    def create(self,request):
        data2=self.request.data['asiento']
        if(isinstance(data2, list)):
            my_list = []
            for valor in data2 :
                request.data['asiento']=valor
                my_list.append(request.data)
                serializer = Bus_PasajeroSerializer(data=request.data)
                if serializer.is_valid():
                    self.object = serializer.save()
                    headers = self.get_success_headers(serializer.data)
            
            return Response(my_list, status=status.HTTP_200_OK)
          

class TrayectoBusInfoViewSet(viewsets.ModelViewSet):
    serializer_class= TrayectoBusInfoSerializer

    def get_queryset(self):
        queryset = Trayecto_Bus.objects.all()
        id_trayecto = self.request.query_params.get('id_trayecto', None)
        if id_trayecto is not None:
            queryset = Trayecto_Bus.objects.filter(id_trayecto=id_trayecto) 
        return queryset   
    
       

   

