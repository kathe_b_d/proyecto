from .models import Trayecto,Trayecto_Bus,Bus_Pasajero,Pasajero
from buses.models import Bus
from rest_framework import serializers


class PasajerosSerializer(serializers.ModelSerializer):
    

    class Meta:
        model = Pasajero
        fields = '__all__'



class TrayectoSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Trayecto
        fields = '__all__'


class TrayectoSerializer2(serializers.ModelSerializer):
    promedio = serializers.FloatField()

    class Meta:
        model = Trayecto
        fields = (['id_trayecto','origen_trayecto','destino_trayecto','promedio'])



class BusSerializer(serializers.ModelSerializer):
  class Meta:
        model = Bus
        fields = '__all__'
        
class TrayectoBusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trayecto_Bus
        fields = '__all__'

class TrayectoBusSerializer2(serializers.ModelSerializer):
    id_bus = BusSerializer(many=False, read_only=True)
    id_trayecto= TrayectoSerializer(many=False)
    class Meta:
            model = Trayecto_Bus
            fields = fields = '__all__'

class Bus_PasajeroSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = Bus_Pasajero
        fields = '__all__'


class TrayectoInfoSerializer(serializers.ModelSerializer):
    id_bus = BusSerializer(many=False, read_only=True)
    id_trayecto= TrayectoSerializer(many=False)
    
    class Meta:
        model = Trayecto_Bus
        fields = '__all__'

class Bus_PasajeroSerializer2(serializers.ModelSerializer):
    id_bus_trayecto=TrayectoInfoSerializer (many=False)
    id_pasajero= PasajerosSerializer(many=False)

    capacidad=serializers.FloatField()
    _id_trayecto=serializers.CharField()
    class Meta:
        model = Bus_Pasajero
        fields = (['id_bus_pasajero','fecha','id_pasajero','id_bus_trayecto','_id_trayecto','asiento','capacidad'])





class TrayectoBusInfoSerializer(serializers.ModelSerializer):
    id_bus = BusSerializer(many=False, read_only=True)
    id_trayecto= TrayectoSerializer(many=False, read_only=True)
    
    class Meta:
        model = Trayecto_Bus
        fields = '__all__'


