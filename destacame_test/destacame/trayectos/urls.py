from rest_framework import routers
from trayectos.views import TrayectoViewSet,TrayectoBusViewSet,TrayectoBusInfoViewSet,BusPasajeroViewSet,BusPasajeroNViewSet,TrayectoViewSet2,TrayectoBusViewSet2,BusPasajeroViewSet_2

router = routers.SimpleRouter()
router.register('trayectos', TrayectoViewSet, base_name ='trayectos')
router.register('trayectos_bus', TrayectoBusViewSet, base_name ='trayectos_bus')
router.register('bus_capacidad', TrayectoBusViewSet2, base_name ='bus_capacidad')
router.register('trayectos_bus_lista', TrayectoBusInfoViewSet, base_name ='trayectos_bus_lista')
router.register('bus_pasajeros', BusPasajeroViewSet, base_name ='bus_pasajeros')
router.register('bus_pasajeros_V', BusPasajeroViewSet_2, base_name ='bus_pasajeros_V')

router.register('bus_pasajeros_n', BusPasajeroNViewSet, base_name ='bus_pasajeros')
router.register('trayecto_promedio', TrayectoViewSet2, base_name ='trayecto_promedio')



urlpatterns = router.urls