from django.apps import AppConfig


class TrayectosConfig(AppConfig):
    name = 'trayectos'
