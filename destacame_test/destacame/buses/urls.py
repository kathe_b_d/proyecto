from rest_framework import routers
from buses.views import BusViewSet,BusChoferViewSet

router = routers.SimpleRouter()
router.register('buses', BusViewSet, base_name ='bus')
router.register('buses_lista', BusChoferViewSet, base_name ='buses_lista')

urlpatterns = router.urls