from django.db import models
from choferes.models import Chofer

# Create your models here.
class Bus(models.Model):
    
    id_bus = models.AutoField(primary_key=True)
    chofer = models.OneToOneField(Chofer,on_delete=models.CASCADE)
    marca = models.CharField(max_length=50, verbose_name="Marca")
    modelo = models.CharField(max_length=50, verbose_name="Modelo")
    placa = models.CharField(max_length=10, unique= True,verbose_name="Placa")
    cantidad_puestos = models.IntegerField(verbose_name= "Puestos")
    fecha_creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    fecha_actualizado = models.DateTimeField(auto_now=True, verbose_name="Fecha de actualización")

    class Meta: 
        verbose_name = 'bus'
        verbose_name_plural = 'buses'
        ordering = ['-fecha_creado']

    def _str_(self):
        return self.placa
