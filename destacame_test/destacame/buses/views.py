from rest_framework import generics

from .models import Bus
from .serializers import BusSerializer,BusChoferSerializer

from rest_framework import viewsets
from rest_framework.views import APIView

# Create your views here.

class BusViewSet(viewsets.ModelViewSet):
    queryset = Bus.objects.all()
    serializer_class= BusSerializer

    
    
class BusChoferViewSet(viewsets.ModelViewSet):
    queryset = Bus.objects.all()
    serializer_class= BusChoferSerializer

   
