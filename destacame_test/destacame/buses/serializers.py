from .models import Bus
from choferes.models import Chofer
from rest_framework import serializers


class BusSerializer(serializers.ModelSerializer):
  class Meta:
        model = Bus
        fields = '__all__'

class ChoferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chofer
        fields = '__all__'

class BusChoferSerializer(serializers.ModelSerializer):
    chofer = ChoferSerializer(many=False, read_only=True)

    class Meta:
        model = Bus
        fields = '__all__'