from .models import Pasajero
from rest_framework import serializers

class PasajerosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pasajero
        fields = '__all__'