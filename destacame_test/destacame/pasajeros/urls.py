from rest_framework import routers
from pasajeros.views import PasajeroViewSet

router = routers.SimpleRouter()
router.register('pasajeros', PasajeroViewSet, base_name ='pasajero')

urlpatterns = router.urls