from rest_framework import generics

from .models import Pasajero
from .serializers import PasajerosSerializer
from rest_framework import viewsets

# Create your views here.

class PasajeroViewSet(viewsets.ModelViewSet):
    queryset = Pasajero.objects.all()
    serializer_class= PasajerosSerializer
    
