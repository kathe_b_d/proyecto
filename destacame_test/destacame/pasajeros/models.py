from django.db import models

# Create your models here.
class Pasajero(models.Model):
    id_pasajero = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, verbose_name="Nombre")
    apellido_paterno = models.CharField(max_length=50, verbose_name="Apellido Paterno")
    apellido_materno = models.CharField(max_length=50, verbose_name="Apellido Materno")
    rut = models.CharField(max_length=11, unique=True, verbose_name="Rut")
    email = models.EmailField(max_length=100, verbose_name= "Correo Electrónico")
    telefono =  models.IntegerField(verbose_name="Teléfono")
    fecha_creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    fecha_actualizado = models.DateTimeField(auto_now=True, verbose_name="Fecha de actualización")

    class Meta: 
        verbose_name = 'pasajero'
        verbose_name_plural = 'pasajeros'
        ordering = ['-fecha_creado']

    def _str_(self):
        return self.nombre+self.apellido_paterno
