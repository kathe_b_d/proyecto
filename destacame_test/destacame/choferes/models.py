from django.db import models

# Create your models here.

class Chofer(models.Model):
    id_chofer = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, unique= True, verbose_name="Nombre")
    apellido_paterno = models.CharField(max_length=50, verbose_name="Apellido Paterno")
    apellido_materno = models.CharField(max_length=50, verbose_name="Apellido Materno")
    rut = models.CharField(max_length=11, unique= True, verbose_name="Rut")
    telefono =  models.IntegerField(verbose_name="Teléfono")
    fecha_creado = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    fecha_actualizado = models.DateTimeField(auto_now=True, verbose_name="Fecha de actualización")

    class Meta: 
        verbose_name = 'chofer'
        verbose_name_plural = 'choferes'
        ordering = ['-fecha_creado']

    def _str_(self):
        return self.nombre