from django.apps import AppConfig


class ChoferesConfig(AppConfig):
    name = 'choferes'
