from rest_framework import generics

from .models import Chofer
from .serializers import ChoferSerializer
from rest_framework import viewsets

# Create your views here.

class ChoferViewSet(viewsets.ModelViewSet):
    queryset = Chofer.objects.all()
    serializer_class= ChoferSerializer
    
