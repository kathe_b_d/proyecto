from rest_framework import routers
from choferes.views import ChoferViewSet

router = routers.SimpleRouter()
router.register('choferes', ChoferViewSet, base_name ='chofer')

urlpatterns = router.urls