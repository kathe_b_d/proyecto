# Proyecto Destacame

El sistema fue desarrollado en sistema operativo Windows, en un entorno virtual con Django Framework.

## Instalación y entorno virtual

1.  Instalar el pyhton 3.7.3, en caso de no poseerlo en equipo local 

2. Descargar el proyecto

4.  Para activar el entorno virtual

        En Windows:
        -ingresar en el directorio => cd destacame_test/env/Script/
        -ejecutar el comando => activate.bat
        En  linux:
        -Ingresar en el directorio destacame_test
        -crear un entorno virtual con los siguientes comandos 
            1.- pip install virtualenv (en caso de no tener instalado el virtualenv)
            2.- virtualenv env
        -Activar el entorno
            3.- source env/bin/activate
        
        

## Activación de paquetes

1.  Ingresar en el directorio raiz proyecto
2. Ejecutar el comando:


    pip install -r requirements.txt


## Base de Datos

La base de datos implementada es mysql, se debe crear una nueva base de datos y registrar los parametros de configuración en el archivo settings.py ubicado en la siguiente ruta


>Ruta: destacame_test/destacame/destacame/
>Parametros de configuración: 

>   'ENGINE': 'django.db.backends.mysql', 
>   'NAME': 'buses',
>   'USER': 'root',
>   'PASSWORD': '',
>   'HOST': 'localhost',   
>   'PORT': '3306',


## Ejecutar las migraciones

Se deben ejecutar los siguientes comandos para que se creen las tablas en base de datos:

1.  python3 manage.py makemigrations
2.  python3 manage.py migrate


## Inicializar el servidor

1.  python3 manage.py runserver


## Acceder a la aplicación

Para acceder al proyecto se debe ingresar a la siguiente ruta por el explorador web
http://localhost:8080/

